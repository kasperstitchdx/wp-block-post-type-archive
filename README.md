# Post Type Archive Block

## Filters (JS)
`narwhal.blocks.postTypeArchive.allowedPostTypes`
* Default value: `[]`
* Defines which post types are allowed to be used in this block.
* If empty, all public post types will be displayed.

`narwhal.blocks.postTypeArchive.postTypeOptions`
* Default value: array of option objects
* Allows adding custom options that aren't actually post types (could be a collection of post types).
* Allows removing options before they are displayed.

## Filters (PHP)
`narwhal/blocks/postTypeArchive/content`
* Default value: empty string
* Expects a string of HTML markup to be returned. This string will be output by WordPress.