<?php

namespace Stitchdx\WordPress\Blocks\PostTypeArchive;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

// Add actions.
add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\\registerBlockEditorAssets', 5 );
//add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\enqueueBlockFrontendScripts', 5 );
add_action( 'init', __NAMESPACE__ . '\\registerBlockType' );

/**
 * Returns the block version number.
 *
 * @return string
 */
function version() {
	return '1.0';
}

/**
 * Returns the name of the block.
 *
 * @return string
 */
function blockName() {
	return 'post-type-archive';
}

/**
 * Enqueues styles and scripts for the admin editor.
 */
function registerBlockEditorAssets() {
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	$fileName = blockName();

//	wp_enqueue_style( "narwhal-block-{$fileName}-editor", assetUrl( "assets/css/{$fileName}-editor{$suffix}.css" ), [], version() );

	wp_enqueue_script( "narwhal-block-{$fileName}-editor", assetUrl( "assets/js/{$fileName}-editor{$suffix}.js" ), [
		'wp-blocks',
		'wp-components',
		'wp-compose',
		'wp-data',
		'wp-editor',
		'wp-element',
		'wp-hooks',
		'wp-i18n',
	], version(), true );
}

/**
 * Enqueues styles and scripts for the frontend.
 */
function enqueueBlockFrontendScripts() {
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	$fileName = blockName();

	wp_enqueue_style( "narwhal-block-{$fileName}", assetUrl( "assets/css/{$fileName}{$suffix}.css" ), [], version() );
}

/**
 * Returns appropriate URL for block assets.
 *
 * @param $path
 *
 * @return mixed
 */
function assetUrl( $path ) {
	if ( defined( 'NARWHAL_BLOCKS_URL' ) ) {
		/**
		 * If NARWHAL_BLOCKS_URL is defined, use it. Should be full URL to the directory containing all the Narwhal
		 * blocks. In a composer setup, it would be the `narwhal-digital` namespace folder.
		 */
		return rtrim( NARWHAL_BLOCKS_URL, '/' ) . '/' . basename( __DIR__ ) . '/' . ltrim( $path, '/' );
	}

	return plugins_url( $path, __FILE__ );
}

/**
 * Registers the block in PHP to allow filtering the output of the block.
 */
function registerBlockType() {
	register_block_type( 'narwhal/' . blockName(), [
		'attributes'      => [
			'postType' => [
				'type' => 'string',
			],
		],
		'render_callback' => function ( array $args ) {
			$postType = '';
			if ( $args['postType'] ) {
				$postType = $args['postType'];

			}

			return apply_filters( 'narwhal/blocks/postTypeArchive/content', '', $postType );
		}
	] );
}