import {__} from '@wordpress/i18n';
import {Button, Disabled, ToolbarButton, Placeholder, SelectControl, Spinner} from '@wordpress/components';
import {compose, withState} from '@wordpress/compose';
import {withSelect} from '@wordpress/data';
import {ServerSideRender, BlockControls} from '@wordpress/editor';
import {Component, Fragment} from '@wordpress/element';
import {applyFilters} from '@wordpress/hooks';
import {blockTitle, blockType} from './config';

let postTypeWhitelist = applyFilters('narwhal.blocks.postTypeArchive.allowedPostTypes', []);

if (0 === postTypeWhitelist.length) {
    postTypeWhitelist = false;
}

class ArchiveEdit extends Component {

    componentDidMount() {
        if (this.props.attributes.postType) {
            this.props.setState({editingPostType: false});
        }
    }

    render() {
        const {editingPostType, setAttributes, setState, postTypes} = this.props;
        const {postType} = this.props.attributes;

        const filteredPostTypes = applyFilters('narwhal.blocks.postTypeArchive.postTypeOptions', postTypes);

        if (!filteredPostTypes || 1 > filteredPostTypes.length) {
            return (
                <Placeholder><Spinner /></Placeholder>
            );
        }

        if (1 === filteredPostTypes.length) {
            if (postType !== filteredPostTypes[0].value) {
                setAttributes({postType: filteredPostTypes[0].value});
            }
        }

        if (editingPostType) {
            return (
                <Placeholder label={blockTitle}>
                    <SelectControl
                        label={__('Post type:', 'narwhal')}
                        onChange={postType => setAttributes({postType})}
                        options={filteredPostTypes}
                        value={postType}
                    />
                    <Button
                        isDefault
                        onClick={() => setState({editingPostType: false})}
                    >{__('Done', 'narwhal')}</Button>
                </Placeholder>
            );
        }

        return (
            <Fragment>
                <Disabled>
                    <ServerSideRender block={blockType} attributes={this.props.attributes} />
                </Disabled>
                <BlockControls>
                    <ToolbarButton
                        icon="edit"
                        onClick={() => setState({editingPostType: true})}
                        title={__('Change post type', 'narwhal')}
                    />
                </BlockControls>
            </Fragment>
        );
    }

}

const Edit = compose(
    withState({
        editingPostType: true,
    }),
    withSelect(
        (select) => {
            const core = select('core');
            const rawPostTypes = core.getPostTypes({per_page: 100});
            const postTypes = [
                {
                    label: __('-- Select post type --', 'narwhal'),
                    value: '',
                }
            ];

            if (rawPostTypes && Array.isArray(rawPostTypes)) {
                rawPostTypes.forEach((obj) => {
                    if (obj.viewable) {
                        if (!postTypeWhitelist || -1 !== _.indexOf(postTypeWhitelist, obj.slug)) {
                            postTypes.push({
                                label: obj.name,
                                value: obj.slug,
                            });
                        }
                    }
                });
            }

            return {
                postTypes,
            };
        }
    )
)(ArchiveEdit);

export {Edit};
