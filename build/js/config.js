import {__} from '@wordpress/i18n';

// Editable
export const namespace = 'narwhal';
export const blockName = 'post-type-archive';
export const blockTitle = __('Post Type Archive', 'narwhal');

// Auto-Generated
export const blockType = `${namespace}/${blockName}`;
export const baseClassName = `wp-block-${namespace}-${blockName}`;
