import {registerBlockType} from '@wordpress/blocks';
import {__} from '@wordpress/i18n';
import {blockTitle, blockType} from './config';
import {Edit} from './edit';

registerBlockType(blockType, {
    title: blockTitle,
    description: __('Archive listing for specific post types.', 'narwhal'),
    icon: 'slides',
    category: 'layout',
    attributes: {
        postType: {},
    },
    edit: Edit,
    // All output is handled via PHP.
    save: () => null,
    supports: {
        anchor: false,
        html: false,
        multiple: false,
    },
});
